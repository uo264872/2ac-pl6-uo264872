#include <stdio.h>
#include <stdlib.h>


int main() {

    printf("Program starts\n");

    int *b = (int *) 0x1234;
    char a = '3';
    char *p = &a;
    int *q = b;

//    __asm__ (
  //      "cli"
  //  );

     if (*p != *q)
        printf("NOT EQUALS and  p-> %c\n", *p);
    else
        printf("EQUALS and q-> %d\n", *q);

    printf("Program ends\n");

    return 0;
}
