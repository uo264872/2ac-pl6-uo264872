#include <stdio.h>

int main()
{

	int i = 3;
	float f = 2.4;
	char * s = "Content";
	//print the content of i
	printf("Value of i: %d\n",i);
	//print the content of f
	printf("Value of f: %f\n",f);
	//print the content of s
	printf("Value of s: %s\n",s);
	return 0;

}
