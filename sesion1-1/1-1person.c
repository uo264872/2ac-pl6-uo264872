#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};
// This abbreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[])
{
    Person Javier;
    PPerson pJavier;
    
    pJavier = &Javier; 
    strcpy(Javier.name, "Javier");
    Javier.heightcm = 180;
    // Assign the weight
    Javier.weightkg = 78.7;
    pJavier->weightkg = 83.2;
    // Show the information of the Peter data structure on the screen
    printf("Javier's height: %d cm; Javier's weight: %f kg",Javier.heightcm, Javier.weightkg);
    return 0;
}
