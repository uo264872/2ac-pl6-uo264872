#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define TRUE 1
#define FALSE 0

const unsigned int N = 10000;
void Task(unsigned int);

void MatrixInverse_A(unsigned int, double **);
void MatrixInverse_B(unsigned int, double **);

double ** MatrixInitialization(unsigned int, int, unsigned int);
void DestroyMatrix(unsigned int, double **);


/**********************************************
* MAIN
**********************************************/
int main(int argc, char* argv[])
{
	Task(N);
	return 0;
}

/**********************************************
* TASK
**********************************************/
void Task(unsigned int nElements)
{
	double	** matrix;

	// Initialize matrix
	matrix = MatrixInitialization(nElements, TRUE, time(NULL));

	if (!matrix)
	{
		fprintf(stderr, "ERROR in Task: Cannot allocate memory\n");
		exit(EXIT_FAILURE);
	}

	// Inverse
	MatrixInverse_A(nElements, matrix);
	MatrixInverse_B(nElements, matrix);

	// Destroy matrix
	DestroyMatrix(nElements, matrix);
}


double getRandom(unsigned int min, unsigned int max)
{
	// Pseudo-random numbers in the interval [min,max]
	return min + (max - min) * ((double)rand()/(double)RAND_MAX);
}

double ** MatrixInitialization(unsigned int nElements, int random, unsigned int seed)
{
	double ** matrix = (double **) malloc(nElements * sizeof(double *));
	if (!matrix)
		return NULL;

	for (int i = 0; i < nElements; i++)
	{
		matrix[i] = (double *) malloc(nElements * sizeof(double));
		if (!matrix)
			return NULL;

		if (random)
		{
			for (int j = 0; j < nElements; j++)
			{
				// Pseudo-random numbers in the interval [1.0-2.0]
				matrix[i][j] = getRandom(1,2);
			}
		}
	}

	return matrix;
}


void MatrixInverse_A(unsigned int nElements, double ** M)
{
	double temp;
	
	for (int i = 0; i < nElements; i++)
	{
		for (int j = i+1; j < nElements; j++)
		{
			temp = M[i][j];
			M[i][j] = M[j][i];
			M[j][i] = temp;
		}
	}
}

void MatrixInverse_B(unsigned int nElements, double ** M)
{
	double	** matrixTemp;
	
	// Initialize matrix
	matrixTemp = MatrixInitialization(nElements, FALSE, 0);
	if (!matrixTemp)
	{
		fprintf(stderr, "ERROR in Task: Cannot allocate memory\n");
		exit(EXIT_FAILURE);
	}
	
	for (int i = 0; i < nElements; i++)
	{
		for (int j = i; j < nElements; j++)
		{
			if (i = j) matrixTemp[i][j] = M[i][j];
			matrixTemp[i][j] = M[j][i];
			matrixTemp[j][i] = M[i][j];
		}
	}
	
	for (int i = 0; i < nElements; i++)
	{
		for (int j = i; j < nElements; j++)
		{
			M[i][j] = matrixTemp[i][j];
			M[j][i] = matrixTemp[j][i];
		}
	}
	
	// Destroy matrix
 	DestroyMatrix(nElements, matrixTemp);
}

void DestroyMatrix(unsigned int nElements, double ** M)
{
	for (int i = 0; i < nElements; i++)
	{
		free(M[i]);
	}
    free(M);
}

